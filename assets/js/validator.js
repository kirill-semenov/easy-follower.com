function create_query_string(url, params){
    let query_params = [];
    Object.keys(params).forEach(function(key){
        query_params.push(encodeURIComponent(key)+'='+encodeURIComponent(params[key]));
    });
    let param_string = query_params.join('&');
    return url+'?'+param_string;
}

document.addEventListener('DOMContentLoaded', function () {
    const loadSpinner = document.createElement('img');
    loadSpinner.src = '/assets/images/loading.gif';

    const payButton = document.getElementById('pay-button');
    const payButtonText = payButton.innerText;

    const emailInput = document.querySelector('input[name=customer_email]');
    const emailNotice = document.getElementById('invalid-email-notice');

    const usernameInput = document.querySelector('input[name=customer_username]');
    const usernameNotice = document.getElementById('invalid-username-notice');

    const validationUrl = document.getElementById('validation_url').value;

    payButton.addEventListener('click', function (e) {
        e.preventDefault();

        payButton.innerText = 'Data Validation';
        payButton.appendChild(document.createElement('br'));
        payButton.appendChild(loadSpinner);
        payButton.disabled = true;

        emailInput.readOnly = true;
        emailNotice.hidden = true;
        emailInput.classList.remove('incorrect');

        usernameInput.readOnly = true;
        usernameNotice.hidden = true;
        usernameInput.classList.remove('incorrect');

        const xhr = new XMLHttpRequest();
        const validationFields = {
            'customer_email': emailInput.value,
            'customer_username': usernameInput.value
        };
        xhr.open('GET', create_query_string(validationUrl, validationFields), true);
        xhr.onreadystatechange =function () {
            if (xhr.readyState !== 4) return;
            if (xhr.status === 200){
                const response = JSON.parse(xhr.responseText);

                // emailInput.disabled = false;
                // usernameInput.disabled = false;

                if(response['result'] === 'OK'){
                    document.getElementById('checksum').value = response['checksum'];
                    document.customerForm.submit();
                } else {
                    emailInput.readOnly = false;
                    usernameInput.readOnly = false;
                    if (response['invalid_fields']['customer_email']){
                        document.getElementById('invalid-email-notice').hidden = false;
                        emailInput.classList.add('incorrect')
                    }
                    if (response['invalid_fields']['customer_username']){
                        document.getElementById('invalid-username-notice').hidden = false;
                        usernameInput.classList.add('incorrect')
                    }
                    payButton.innerText = payButtonText;
                    payButton.disabled = false;
                }
            } else {
                window.location = window.location;
            }
        };
        xhr.send();
    });
});