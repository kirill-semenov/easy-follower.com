<?php
$CONFIG = [
    'products' => [
        'twitter' => [
            'followers' => [
                'name' => 'Twitter Followers',
                'prices' => [
                    [
                        'quantity' => 500,
                        'code' => '500TF',
                        'price' => 6.99
                    ],
                    [
                        'quantity' => 1000,
                        'code' => '1000TF',
                        'price' => 9.99
                    ],
                    [
                        'quantity' => 2500,
                        'code' => '2500TF',
                        'price' => 19.99
                    ],
                    [
                        'quantity' => 5000,
                        'code' => '5000TF',
                        'price' => 39.99
                    ]
                ]
            ]
        ]
    ],
    'paypal' => [
        'business' => !defined('DEBUG') ? 'W99A6WW5C7QRL' : 'XZ5PZPVRSRX5C'
    ],

    'safeCharge' => [
        'merchantId' => !defined('DEBUG') ? '' : '7295088273024301073',
        'merchantSiteId' => !defined('DEBUG') ? '' : '160713',
        'merchantApiKey' => !defined('DEBUG') ? '' : 'zEz1uEF9S10vr012a6M7AY3IjebtILm5JmAbWgKWIcCLkDpcWhQNRYtILznoBiYK'
    ],

    'JustAnotherPanelClient' => [
        'APIUrl' => 'https://justanotherpanel.com/api/v2',
        'APIKey' => !defined('DEBUG') ? '0bdf95e76b2d006fdaed559d8feebd18' : 'aa0519f500c2e4b6b8e3e8c6731d5593',
        'services' => [
            'twitter' => [
                'followers' => 711
            ]
        ]
    ],

    'telegram' => [
        'groupId' => '-1001252035848',
        'APIKey' => '586641275:AAH5Uv5HauBbpU7xKlEK_w12_0RFS1ncD5A'
    ],

    'googleSheets' => [
        'credentials' => '{
               "type": "service_account",
               "project_id": "easy-follower-217317",
               "private_key_id": "57d0bc3f6d6947a72c636c7cbbb2a019928d3733",
               "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFmbL9zvpRn3eK\nKgGkRpnbgkFmzoil7fEogu1ijo7iAmtM/nrfRjmpBppDeGO1GHy4BQmq0LCJmZ11\nHuuZUHrHUSQ1n2HtpT2NJzwMcggZKvBzrb3kKfaWzJ2z56/EZopO/0b0dIH2QvU/\nT7hsDKvJ/Ju2uPdGEMXmA0Wg6u6p9pYpjrwOy5Lp8x11fwLpC0YQB5z3KZ7mAhra\nnmksZ21J/Q1TbA2ZGKGQwMCD+xaI3oMMlUb2ZA8i5o3jOqCGJFmHWXP3iSLa3Asc\nHfTsgZDGsCzBhfSxJkw4Danr4l+4HWCuGPXfOvB2SuaqcGR81kSLysFkorji9cTn\nkJgHZhwzAgMBAAECggEAGivqH/a8Lc0RZVHR/NhDBDwDfeKIs8geQvowXHeQPNFg\nNTGWxErF/qP6u56kP3X6UtnhTBKu8dKlnxUU1FjAoknrd1S1q5GmuN2Dn7cHaTny\n4LIYLrJQ0EFSo+OMYqwde9VmKEuOyQyuSeU9AnFH3DgNHGLxkVdNttIjyeqZPhZz\neDNnsV5h+gGeYGsQhoaRd3jbOPgSvVECUIQ54piQMQu6GQeTkUInpbLjF+LZLRvP\nkAOF44uUKHP0mLZVrSjUn5ddckJW77RkFfWQgVy4CYuOzB9XNZu4DOUdfpKrx64S\ns0yryd5ulQLU+zl49AudVeAqxDib6E3uLbvBdXJtpQKBgQDmXOzRq46ZyJJSyyQA\nGp4BBBcFbG6U1Tog89uL9M3Y9XJjMFu6hOSBG9X5VLOdigbaxm1DXp+8srROPHFj\nRkvgjn+B9kWv7VE3Qn61yVZEYc/HdrKYb3T155BlryVdDuDbiVdLSTbJE5/fYZXW\n4l39S+b0BOTh5LqChYlhKItYBQKBgQDbl1zkNzQgA77A3O6X2+Co0FDp6OvHhhrb\nK2EW6OkVMI7qyCwpoIwL+Tq4ALKNoHO1ABWfl229oL4XRDHSsW5Q0gANsJkQm71z\n5UqwDTmZr6gtgHpwwqVeTHGnmNBi61LauEquXCAwHKpKlvsUAVU0lMJpmqWPCzF8\n6nPje/lw1wKBgQDcOq507KxRvPyWhnqOVuQDWKOhnb5CL295HWkg7hL1fgGzbgHj\n2TSTcccorJ9i1xgig1zl8UqJHddejE5ieEwGDHU+C/EndZwVVk7JAAinK1N2/7VS\nIgwRIyZa8AyqYJT7jNuVsBLePRK0fhi/7H2qsKvFUgB3LTJZocxqxFYi+QKBgGk1\n1GPjry3HnZV6RTatOq6nYJ0x5AXs0uE/n30CiuqEIXWMAtzO90qlVuTiMKp6vfVy\n3Olh/Epmwa0UF2sDZ/+BHlVwz2WNuO/WZDqFm5tu9f0XGZL9L45Gwg2yweUjKsdw\nlReFPEbQ9SZXwGSNXu2aeKtwVPJ8QnG/Q1czBwhxAoGAKCYmY40OegynSFW6SsDW\nYrVb6RZYeKZRtrRXVQ7VPJWPnsWpKp5nooojYjnlm4rkmJbC2GqjWMfhdAh8FajH\nZUREqASy0tdwyFyF+ppNlUz9VvBGyLC9JXz+cUihQn43Zzwmzl7bv2h4d3K6bzGJ\n5KZuuWWoOR1iUnqR1gPS2UM=\n-----END PRIVATE KEY-----\n",
               "client_email": "easy-follower-watcher@easy-follower-217317.iam.gserviceaccount.com",
               "client_id": "118096736045607394852",
               "auth_uri": "https://accounts.google.com/o/oauth2/auth",
               "token_uri": "https://oauth2.googleapis.com/token",
               "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
               "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/easy-follower-watcher%40easy-follower-217317.iam.gserviceaccount.com"
             }',
        'spreadsheetId' => '1Z-QJAE-fNaVmzvtqQE3OT9vckRuos9rw1dbmDHmEYY0',
        'APIKey' => 'AIzaSyD7B2IuJ4Aj76aVjwNbj9NviZxDkxyDK7c'
    ]
];
