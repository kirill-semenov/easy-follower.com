<?php
namespace Engine;

class BaseController{

    protected $container;

    public function __construct($container){
        $this->container = $container;
    }

}