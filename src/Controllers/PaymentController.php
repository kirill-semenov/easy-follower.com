<?php
namespace Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class PaymentController extends \Engine\BaseController {
    // Processing IPN & DNM notifications

    public function ipn(Request $request, Response $response, Array $args){
        $this->container->paypal->usePHPCerts();
        if($this->container->paypal->verifyIPN()){
            if($_POST['payment_status'] == 'Completed') {
                $customData = json_decode($_POST['custom'], true);
                $this->process_payment(
                    'PayPal',
                    $_POST['txn_id'],
                    $_POST['payment_date'],
                    $customData['customer_email'],
                    $customData['customer_username'],
                    $_POST['mc_gross'],
                    $_POST['mc_currency'],
                    $_POST['item_number']
                );
            }
        }

    }

    public function dmn(Request $request, Response $response, Array $args){
        if($this->container->safeCharge->verifyDMN()){
            if($_REQUEST['ppp_status'] == 'OK'){
                $customData = json_decode($_REQUEST['customData'], true);
                $this->process_payment(
                    'SafeCharge',
                    $_REQUEST['PPP_TransactionID'],
                    $_REQUEST['responseTimeStamp'],
                    $customData['customer_email'],
                    $customData['customer_username'],
                    $_REQUEST['totalAmount'],
                    $_REQUEST['currency'],
                    $_REQUEST['productId']
                );
            }
        }
    }

    private function process_payment($source, $txn_id, $payment_time, $payer_email,
                                  $payer_username, $payment_amount, $currency, $product_id){
        if($this->container->products->validateOrder('twitter', 'followers', $product_id, $payment_amount)){
            $product = $this->container->products->getByCode('twitter', 'followers', $product_id);
            $provider_result = $this->container->provider->addOrder(
                'twitter',
                'followers',
                'https://twitter.com',
                $payer_username,
                $product['quantity']
            );
            $external_id = null;
            if (array_key_exists('error', $provider_result)){
                $this->container->notifier->notify('🆘 JAP:'.$provider_result['error']);
            } else {
                $external_id = $provider_result['order'];
            }
            $this->container->orderTable->append(
                $source,
                $txn_id,
                $payment_time,
                $payer_email,
                $payer_username,
                $payment_amount,
                $currency,
                $product['name'],
                $external_id
            );
            $this->container->notifier->notify(
                "🔷 New order {$txn_id}\n"
                ."Price: {$payment_amount} {$currency}\n\n"
                .$this->container->orderTable->getSheetURL()
            );
        }
    }

}