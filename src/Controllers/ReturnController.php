<?php
namespace Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ReturnController extends \Engine\BaseController {
    // Return pages controller

    public function success(Request $request, Response $response, Array $args){
        if(empty($_REQUEST['ppp_status'])) {
            $customData = json_decode($_REQUEST['custom'], true);
            $product = $this->container->products->getByCode('twitter', 'followers', $_REQUEST['item_number']);
        }
        else{
            $customData = json_decode($_REQUEST['customData'], true);
            $product = $this->container->products->getByCode('twitter', 'followers', $_REQUEST['productId']);
        }
        return $this->container->view->render($response, 'return.twig', [
            'success'=> true,
            'product' => $product,
            'customer_username' => $customData['customer_username'],
            'email' => $customData['customer_email'],
            'price' => empty($_REQUEST['totalAmount']) ? $_REQUEST['mc_gross'] : $_REQUEST['totalAmount'],
            'currency' => empty($_REQUEST['currency']) ? $_REQUEST['mc_currency'] : $_REQUEST['currency']
        ]);
    }

    public function cancel(Request $request, Response $response, Array $args){
        return $this->container->view->render($response, 'return.twig', [
            'success'=> false
        ]);
    }

}