<?php
namespace Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class InfoController extends \Engine\BaseController {
    // Controller for rendering informational pages

    public function index(Request $request, Response $response, Array $args){
        $products = $this->container->products->getByGroup('twitter', 'followers');
        $max_price_per_one = null;
        foreach ($products as $index => $product){
            $price_per_one = $product['price'] / $product['quantity'];
            if (empty($max_price_per_one) || $max_price_per_one < $price_per_one){
                $max_price_per_one = $price_per_one;
            }
            $products[$index]['old_price'] = $max_price_per_one * $product['quantity'];
        }
        return $this->container->view->render($response, 'index.twig', ['products' => $products]);
    }

    public function terms(Request $request, Response $response, Array $args){
        return $this->container->view->render($response, 'terms.twig');
    }

}