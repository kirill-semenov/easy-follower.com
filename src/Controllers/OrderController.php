<?php
namespace Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class OrderController extends \Engine\BaseController {
    // Order & Payment pages controller

    private $paymentSalt = 'khXi)iI)lGqN9U5T6eNkRnJz%HLR-XQe';

    public function product (Request $request, Response $response, Array $args){
        $product = $this->container->products->safeGet('twitter', 'followers', $args['id']);
        return $this->container->view->render($response, 'order.twig', [
            'product' => $product,
            'index' => $args['id'],
            'customer_username' => isset($_GET['customer_username']) ? $_GET['customer_username'] : null,
            'customer_email' => isset($_GET['customer_email']) ? $_GET['customer_email'] : null,
            'invalid' => isset($_GET['invalid']) ? $_GET['invalid'] : false
        ]);
    }

    public function payment (Request $request, Response $response, Array $args){
        // User data validation

        $customerEmail = $_POST['customer_email'];
        $customerUsername = $_POST['customer_username'];

        if($_POST['checksum'] != md5($this->paymentSalt.$customerEmail.$customerUsername)){
            return $response->withStatus(302)->withHeader(
                'Location',
                $this->container->router->pathFor('root')
            );
        }
        $product = $this->container->products->getByCode('twitter', 'followers', $_POST['product_code']);
        $custom_data = [
            'customer_email' => $customerEmail,
            'customer_username' => $customerUsername
        ];
        return $this->container->view->render($response, 'payment.twig', [
            'product' => $product,
            'customer_email' => $customerEmail,
            'customer_username' => $customerUsername,
            'paypal_payment_url' => $this->container->paypal->payURL(
                $product['name'],
                $product['code'],
                $product['price'],
                json_encode($custom_data)
            ),
            'card_payment_url' => $this->container->safeCharge->payURL(
                $product['name'],
                $product['code'],
                $product['price'],
                json_encode($custom_data)
            )
        ]);
    }

    public function validate(Request $request, Response $response, Array $args){
        $customerEmail = $_GET['customer_email'];
        $customerUsername = $_GET['customer_username'];

        $emailResult = filter_var($customerEmail, FILTER_VALIDATE_EMAIL);
        $usernameResult = $customerUsername
            && strpos(get_headers("https://twitter.com/{$customerUsername}")[0], '200');

        if (!($emailResult && $usernameResult)){
            $response->getBody()->write(json_encode(
                [
                    'result' => 'ERROR',
                    'invalid_fields' => [
                        'customer_email' => !$emailResult,
                        'customer_username' => !$usernameResult
                    ]
                ]
            ));
        } else {
            $response->getBody()->write(json_encode(
                [
                    'result' => 'OK',
                    'checksum' => md5($this->paymentSalt.$customerEmail.$customerUsername)
                ]
            ));
        }
        return $response;
    }

}