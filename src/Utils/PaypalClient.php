<?php
namespace Utils;

class PaypalClient
{

    private $use_sandbox = false;

    private $use_local_certs = true;

    const VERIFY_URI = 'https://ipnpb.paypal.com/cgi-bin/webscr';

    const SANDBOX_VERIFY_URI = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';

    const VALID = 'VERIFIED';

    const INVALID = 'INVALID';

    private $main_url;
    private $business;

    public function __construct($business){
        $this->main_url = $_SERVER['SERVER_NAME'];
        $this->business = $business;
    }

    private function getApiUrl(){
        if (!defined('DEBUG')){
            return "https://www.paypal.com/cgi-bin/webscr";
        } else {
            return "https://www.sandbox.paypal.com/cgi-bin/webscr";
        }
    }

    public function useSandbox()
    {
        $this->use_sandbox = true;
    }

    public function usePHPCerts()
    {
        $this->use_local_certs = false;
    }

    public function getPaypalUri()
    {
        if ($this->use_sandbox) {
            return self::SANDBOX_VERIFY_URI;
        } else {
            return self::VERIFY_URI;
        }
    }

    public function verifyIPN()
    {
        if (defined('DEBUG')){
            $this->useSandbox();
        }
        if ( ! count($_POST)) {
            throw new \Exception("Missing POST Data");
        }
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                if ($keyval[0] === 'payment_date') {
                    if (substr_count($keyval[1], '+') === 1) {
                        $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                    }
                }
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
        // Build the body of the verification post request, adding the _notify-validate command.
        $req = 'cmd=_notify-validate';
        $get_magic_quotes_exists = false;
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
        $ch = curl_init($this->getPaypalUri());
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // This is often required if the server is missing a global cert bundle, or is using an outdated one.
        if ($this->use_local_certs) {
            curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
        }
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: PHP-IPN-Verification-Script',
            'Connection: Close',
        ));
        $res = curl_exec($ch);
        if ( ! ($res)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            curl_close($ch);
            throw new \Exception("cURL error: [$errno] $errstr");
        }
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];
        if ($http_code != 200) {
            throw new \Exception("PayPal responded with http code $http_code");
        }
        curl_close($ch);
        // Check if PayPal verifies the IPN data, and if so, return true.
        if ($res == self::VALID) {
            return true;
        } else {
            return false;
        }
    }

    public function payURL($product_name, $product_id, $total_amount, $custom_data){
        $params =  array(
            'cmd' => '_xclick',
            'business' => $this->business,
            'lc' => 'US',
            'item_name' => $product_name,
            'item_number' => $product_id,
            'amount' => $total_amount,
            'currency_code' => 'USD',
            'button_subtype' => 'services',
            'no_note' => '1',
            'no_shipping' => '1',
            'notify_url' => "http://{$this->main_url}/ipn",
            'return' => "https://{$this->main_url}/success",
            'cancel_return' => "https://{$this->main_url}/cancel",
            'rm' => '2',
            'bn' => "PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted",
            'custom' => $custom_data
        );

        array_map('trim' , $params);

        $fields_string="";
        foreach($params as $key=>$value) { $fields_string .= $key."=".urlencode($value)."&"; }
        $fields_string = rtrim($fields_string, "&");

        return $this->getApiUrl().'?'.$fields_string;
    }
}