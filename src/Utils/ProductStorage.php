<?php
namespace Utils;

class ProductStorage{

    private $products;

    public function __construct($products){
        $this->products = $products;
    }

    public function getByGroup($product_group, $product_subgroup){
        $suitable_products = $this->products[$product_group][$product_subgroup];
        $result_products = $suitable_products['prices'];
        foreach ($result_products as $index => $product){
            $result_products[$index]['name'] = $product['quantity'].' '.$suitable_products['name'];
        }
        return $result_products;
    }

    public function safeGet($product_group, $product_subgroup, $id=0){
        $suitable_products = $this->getByGroup($product_group, $product_subgroup);
        $product =  empty($suitable_products[$id]) ? $suitable_products[0] : $suitable_products[$id];

        return $product;
    }

    public function getByCode($product_group, $product_subgroup, $code){
        $group_products = $this->getByGroup($product_group, $product_subgroup);
        foreach ($group_products as $product){
            if ($product['code'] == $code){
                return $product;
            }
        }
        return null;
    }

    public function validateOrder($product_group, $product_subgroup, $code, $price){
        $product = $this->getByCode($product_group, $product_subgroup, $code);
        if ($product['price'] == $price) {
            return true;
        }
        return false;
    }
}

