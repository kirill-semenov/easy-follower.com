<?php
namespace Utils;

class SafeChargeClient{

    private $main_url;
    private $APIKey;
    private $merchantId;
    private $merchantSiteId;

    public function __construct($APIKey, $merchantId, $merchantSiteId){
        $this->main_url = 'https://'.$_SERVER['SERVER_NAME'];
        $this->APIKey = $APIKey;
        $this->merchantId = $merchantId;
        $this->merchantSiteId = $merchantSiteId;
    }

    private function getApiUrl(){
        if (!defined('DEBUG')){
            return "https://secure.safecharge.com/ppp/purchase.do?";
        } else {
            return "https://ppp-test.safecharge.com/ppp/purchase.do?";
        }
    }

    public function verifyDMN(){
        $requestCheckSum = hash(
            'sha256',
            $this->APIKey
            .$_REQUEST['totalAmount']
            .$_REQUEST['currency']
            .$_REQUEST['responseTimeStamp']
            .$_REQUEST['PPP_TransactionID']
            .$_REQUEST['Status']
            .$_REQUEST['productId']
        );
        return $requestCheckSum == $_REQUEST['advanceResponseChecksum'];
    }

    public function payURL($product_name, $product_id, $total_amount, $custom_data){
        $params =  array(
            'currency' => 'USD',
            'item_name_1' => $product_name,
            'item_number_1' => '1',
            'item_quantity_1' => '1',
            'item_amount_1' => $total_amount,
            'numberofitems' => '1',
            'encoding' => 'utf-8',
            'productId' => $product_id,
            'merchant_id' => $this->merchantId,
            'merchant_site_id' => $this->merchantSiteId,
            'time_stamp' => date('Y-m-d H:i:s'),
            'version' => '4.0.0',
            'user_token' => 'auto',
            'total_amount' => $total_amount,
            'notify_url' => "{$this->main_url}/dmn",
            'success_url' => "{$this->main_url}/success",
            'failure_url' => "{$this->main_url}/cancel",
            'back_url' => "{$this->main_url}/cancel",
            'customData' => $custom_data
        );

        array_map('trim' , $params);

        $JoinedInfo = join('' , array_values($params));

        $params["checksum"] = hash("sha256" ,$this->APIKey.$JoinedInfo);

        $fields_string="";
        foreach($params as $key=>$value) { $fields_string .= $key."=".urlencode($value)."&"; }
        $fields_string = rtrim($fields_string, "&");

        return $this->getApiUrl().$fields_string;
    }
}