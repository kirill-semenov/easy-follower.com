<?php
namespace Utils;

class JustAnotherPanelClient{

    private $APIKey;
    private $APIUrl;
    private $services;

    public function __construct($api_key, $api_url, $services){
        $this->APIKey = $api_key;
        $this->APIUrl = $api_url;
        $this->services = $services;
    }

    private function send($data) {
        $_post = Array();
        if (is_array($data)) {
            foreach ($data as $name => $value) {
                $_post[] = $name.'='.urlencode($value);
            }
        }

        $ch = curl_init($this->APIUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if (is_array($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);
        if (curl_errno($ch) != 0 && empty($result)) {
            $result = false;
        }
        curl_close($ch);
        error_log($result);
        return json_decode($result, true);
    }

    public function addOrder($service_group, $service_subgroup, $target_soc_url, $username, $quantity){
        $link = $target_soc_url.'/'.$username;
        return $this->send([
            'key' => $this->APIKey,
            'action' => 'add',
            'service' => $this->services[$service_group][$service_subgroup],
            'link' => $link,
            'quantity' => $quantity,
            'username' => $username
        ]);
    }

    public function getServiceId($service_name){
        return $this->services[$service_name];
    }
}