<?php
namespace Utils;

class TelegramNotifier{

    private $APIKey;
    private $chatId;

    public function __construct($APIKey, $chatId){
        $this->APIKey = $APIKey;
        $this->chatId = $chatId;
    }

    public function notify($message){
        $message = urlencode($message);
        file_get_contents(
            "https://api.telegram.org/bot"
            .$this->APIKey
            ."/sendMessage?chat_id="
            .$this->chatId
            ."&text={$message}"
        );
    }
}