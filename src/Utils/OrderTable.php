<?php
namespace Utils;

class OrderTable{

    private $APIKey;
    private $credentials;
    private $spreadsheetId;


    public function __construct($APIKey, $credentials, $spreadsheetId){
        $this->APIKey = $APIKey;
        $this->credentials = $credentials;
        $this->spreadsheetId = $spreadsheetId;
    }

    public function append($source, $txn_id, $pay_time, $payer_email, $twitter_username, $payment_sum, $currency, $product_name, $ext_id=null){
        $client = new \Google_Client();
        $client->setApplicationName('EasyFollower');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');
        $client->setAccessToken($this->APIKey);

        $client->setAuthConfig(json_decode($this->credentials, true));

        $sheets = new \Google_Service_Sheets($client);
        $body = new \Google_Service_Sheets_ValueRange(['values' => [
            [$source, $txn_id, $pay_time, $payer_email, $twitter_username, $payment_sum, $currency, $product_name, $ext_id]
        ]
        ]);
        $sheets->spreadsheets_values->append($this->spreadsheetId, 'B1:3', $body, ["valueInputOption" => "RAW"]);

    }

    public function getSheetURL(){
        return "https://docs.google.com/spreadsheets/d/{$this->spreadsheetId}";
    }
}
