<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require './vendor/autoload.php';

if ($_SERVER['SERVER_NAME'] != 'easy-follower.com'){
    define('DEBUG', true);
}

require_once 'config.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => defined('DEBUG')
    ]
]);

////////////////////// Dependency Initializing ///////////////////////////
$container = $app->getContainer();
$container['view'] = function ($c){
    $view = new \Slim\Views\Twig('templates', [
        'cache' => 'cache',
        'debug' => defined('DEBUG')
    ]);
    $view->getEnvironment()->addGlobal('debug', defined('DEBUG'));
    $router = $c['router'];
    $url = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $url));
    return $view;
};

$container['paypal'] = new Utils\PaypalClient(
    $CONFIG['paypal']['business']
);
$container['safeCharge'] = new Utils\SafeChargeClient(
    $CONFIG['safeCharge']['merchantApiKey'],
    $CONFIG['safeCharge']['merchantId'],
    $CONFIG['safeCharge']['merchantSiteId']
);
$container['orderTable'] = new Utils\OrderTable(
    $CONFIG['googleSheets']['APIKey'],
    $CONFIG['googleSheets']['credentials'],
    $CONFIG['googleSheets']['spreadsheetId']
);
$container['notifier'] = new Utils\TelegramNotifier(
    $CONFIG['telegram']['APIKey'],
    $CONFIG['telegram']['groupId']
);
$container['provider'] = new Utils\JustAnotherPanelClient(
    $CONFIG['JustAnotherPanelClient']['APIKey'],
    $CONFIG['JustAnotherPanelClient']['APIUrl'],
    $CONFIG['JustAnotherPanelClient']['services']
);
$container['products'] = new Utils\ProductStorage($CONFIG['products']);

////////////////////// Set routing rules /////////////////////////////
$app->get('/', Controllers\InfoController::class.':index')->setName('root');
$app->get('/product/{id}/', Controllers\OrderController::class.':product')->setName('product');
$app->post('/payment', \Controllers\OrderController::class.':payment')->setName('payment');
$app->get('/validate', \Controllers\OrderController::class.':validate')->setName('validate');
$app->any('/success', Controllers\ReturnController::class.':success');
$app->get('/cancel', Controllers\ReturnController::class.':cancel');
$app->post('/ipn', Controllers\PaymentController::class.':ipn')->setName('ipn');
$app->any('/dmn', Controllers\PaymentController::class.':dmn')->setName('dmn');
$app->get('/terms', Controllers\InfoController::class.':terms')->setName('terms');

$app->run();
